/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.heyl.processors.colin;

import org.apache.commons.io.IOUtils;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnShutdown;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.annotation.lifecycle.OnUnscheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.components.PropertyValue;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.StreamCallback;
import org.apache.nifi.processor.util.StandardValidators;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.*;

@Tags({"example, test, colin"})
@CapabilityDescription("This Processor reverses the content of a FlowFile if the 'reversed' property is set to true")
public class ContentReverserProcessor extends AbstractProcessor {

    public static final PropertyDescriptor REVERSED = new PropertyDescriptor.Builder()
            .name("REVERSED")
            .displayName("Reversed")
            .description("A boolean that determines whether or not the content should be reversed")
            .required(true)
            .addValidator(StandardValidators.BOOLEAN_VALIDATOR)
            .defaultValue("false")
            .build();

    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("REL_SUCCESS")
            .description("Successful")
            .build();

    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("REL_FAILURE")
            .description("Failed")
            .build();

    private List<PropertyDescriptor> propertyDescriptors;
    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        /*
         This method is called after a NiFi Processor is created. It is run AFTER the object is
         created, and AFTER any method with the @OnAdded annotation.
         */
        propertyDescriptors = new ArrayList<>();
        propertyDescriptors.add(REVERSED);
        this.propertyDescriptors = Collections.unmodifiableList(propertyDescriptors);

        relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    /*
      The following 4 methods are not used by this Processor, but I have included them
      to serve as an example of the various different annotations NiFi provides.
     */
    @OnScheduled
    public void onScheduled(final ProcessContext context) {
        /*
          This method is called when a NiFi Processor is scheduled to run:
          i.e. when a user presses the green "play" icon on the processor.
         */
    }

    @OnUnscheduled
    public void onUnscheduled(final ProcessContext context) {
        /*
          This method is called when a NiFi Processor is in the process of stopping:
          i.e. immediately after a user presses the red "stop" icon on the processor.
         */
    }

    @OnStopped
    public void onStopped(final ProcessContext context) {
        /*
          This method is called when a NiFi Processor has FINISHED stopping:
          i.e. when the NiFi processor has finished stopping and is now in a state of "stopped".
         */
    }

    @OnShutdown
    public void onShutDown(final ProcessContext context) {
        /*
          This method is called when NiFi is shut down.
          i.e when the user runs "sh nifi.sh stop" in the nifi/bin/ folder
         */
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return propertyDescriptors;
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();

        if (flowFile == null) {
            return;
        }

        // TODO: Write your implementation.
        session.transfer(flowFile, REL_SUCCESS);
    }
}
