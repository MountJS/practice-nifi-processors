package com.heyl.processors.colin;

import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.heyl.processors.colin.ContentReverserProcessor.REL_SUCCESS;
import static com.heyl.processors.colin.ContentReverserProcessor.REVERSED;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

public class ContentReverserProcessorTest {

    private TestRunner testRunner;

    @Before
    public void init() {
        testRunner = TestRunners.newTestRunner(ContentReverserProcessor.class);
    }

    @Test
    public void reversesContentWhenFlagSetToTrue() {
        testRunner.setProperty(REVERSED, "true");
        testRunner.enqueue("hello");
        testRunner.run();

        testRunner.assertAllFlowFilesTransferred(REL_SUCCESS);

        List<MockFlowFile> flowFiles = testRunner.getFlowFilesForRelationship(REL_SUCCESS);
        assertThat(flowFiles, notNullValue());
        assertThat(flowFiles.size(), is(1));
        flowFiles.get(0).assertContentEquals("olleh");
    }

    @Test
    public void doesNotReverseContentWhenFlagSetToFalse() {
        testRunner.setProperty(REVERSED, "false");
        testRunner.enqueue("hello");
        testRunner.run();

        testRunner.assertAllFlowFilesTransferred(REL_SUCCESS);

        List<MockFlowFile> flowFiles = testRunner.getFlowFilesForRelationship(REL_SUCCESS);
        assertThat(flowFiles, notNullValue());
        assertThat(flowFiles.size(), is(1));
        flowFiles.get(0).assertContentEquals("hello");
    }

}