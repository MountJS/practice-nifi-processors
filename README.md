### Practice NiFi Processors

Create a NAR file by running:

```
maven clean install
```

The NAR file will be located at nifi-colin-nar/target/nifi-colin-nar-1.0-SNAPSHOT.nar

This NAR can be placed in nifi/lib/ in order to make the processors in this project available from the NiFi dashboard.